package com.sample.two.util;

import com.sample.two.Department;
import com.sample.two.DroolsRuleServiceImpl;
import com.sample.two.Employee;
import com.sample.two.SalaryResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class DroolsRuleServiceImplTest {

    @Test
    void getSalary(){
        DroolsRuleServiceImpl droolsRuleService = new DroolsRuleServiceImpl();
        Employee employee = new Employee();
        Department department = new Department();
        employee.setEmpId(1);
        department.setDepartmentName("Sales");
        employee.setDepartment(department);
        employee.setYearsOfExperience(4 );
        SalaryResponse salaryResponse = new SalaryResponse();
        Long salary = droolsRuleService.getSalary(employee);
        System.out.println(salary);
        Assertions.assertNotNull(salary);
        Assertions.assertEquals(50000L, salary);
    }

}