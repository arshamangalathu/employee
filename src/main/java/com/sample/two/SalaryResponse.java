package com.sample.two;

import lombok.Data;

@Data
public class SalaryResponse {

    private Long salary;

}
