package com.sample.two;

import lombok.Data;

@Data
public class Department {

    private String departmentName;

}
