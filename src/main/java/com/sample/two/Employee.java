package com.sample.two;

import lombok.Data;

@Data
public class Employee {

    private Integer empId;

    private Department department;

    private Integer yearsOfExperience;

}
